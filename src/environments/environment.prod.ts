export const environment = {
  production: true,

  websiteRoot: JSON.parse((<HTMLInputElement>document.getElementById('__ClientContext')).value).websiteRoot,
  imageUrl: 'areas/ng/Advertiser-Rep-Territory-Mapping/assets/image',
  baseUrl: JSON.parse((<HTMLInputElement>document.getElementById('__ClientContext')).value).baseUrl,
  token: (<HTMLInputElement>document.getElementById('__RequestVerificationToken')).value,
  apiBaseURL: (<HTMLInputElement>document.getElementById('apiUrl')).value,
  CurrentUserName: (<HTMLInputElement>document.getElementById('ctl01_AccountArea_PartyName')).innerText
};

