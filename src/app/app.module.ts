import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgSelectModule } from '@ng-select/ng-select';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AdvertiserRepTerritoryMappingComponent } from './component/advertiser-rep-territory-mapping/advertiser-rep-territory-mapping.component';
import { AdvertiserAgencyMappingComponent } from './component/advertiser-agency-mapping/advertiser-agency-mapping.component';

@NgModule({
  declarations: [
    AppComponent,
    AdvertiserRepTerritoryMappingComponent,
    AdvertiserAgencyMappingComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      preventDuplicates: false,
      autoDismiss: false,
      maxOpened: 0
    }),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    NgSelectModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
