import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { isNullOrUndefined, isNumber } from 'util';
import { NgxSpinnerService } from "ngx-spinner";
import { environment } from '../../../environments/environment';
import { AdvRepTerritoryMappingServiceService } from '../../Service/adv-rep-territory-mapping-service.service';
import {AppComponent} from '../../app.component';
declare var $, jQuery: any;

@Component({
  selector: 'app-advertiser-rep-territory-mapping',
  templateUrl: './advertiser-rep-territory-mapping.component.html',
  styleUrls: ['./advertiser-rep-territory-mapping.component.css']
})
export class AdvertiserRepTerritoryMappingComponent implements OnInit {

  selectedAdvertiser: any;
  AdvertiserRepTerritoryMappingForm: FormGroup;
  isFormSubmitted = false;
  Advertisers_Array = [];
  Reps_Array = [];
  Territory_Array = [];
  advertisersRepTerritoryMappingList = [];
  Territories: any = {};
  fieldArray: Array<any> = [
    {
      'Rep': '',
      'Territory': '',
      'Percentage': '',
      'RepsErrorMsg': '',
      'TerritoryErrorMsg': '',
      'PencentageErrorMsg': '',
      'TerritoryArray': []
    }
  ];

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  advertiserName='';
  advertiserRepButtonName = 'Submit';

  constructor(private formbuilder: FormBuilder,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private appComponent :AppComponent,
    private advRepTerritoryMappingServiceService: AdvRepTerritoryMappingServiceService) {
    this.AdvertiserRepTerritoryMappingForm = this.formbuilder.group({
      Advertiser: [null, Validators.required]
      //,Territory: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.Advertisers_Array = [];
    this.getToken();
    //this.getAdvertiserData(0, []);
    this.getAllReps();

    // //just for testing purpose
    // this.websiteRoot = environment.ApiURL;
    // this.GetAdvertiserForLocalTesting();
    this.get_AdvertiserRepTerritoryMapping_List();
  }

  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;

      var pathArray = this.websiteRoot.split('/');

      var protocol = pathArray[0];
      var host = pathArray[2];

      this.websiteRoot = protocol + '//' + host + '/';

      if (this.websiteRoot.startsWith("http://"))
        this.websiteRoot = this.websiteRoot.replace('http://', 'https://');

      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }
  }

  getAdvertiserData(offset: number, advertisers: any[]) {
    try {
      this.showLoader();
      this.advRepTerritoryMappingServiceService.GetAdvertiser(this.websiteRoot, offset).subscribe(result => {
        if (isNullOrUndefined(result) == false) {
          const ResponseData = result.Items.$values;
          if (ResponseData.length > 0) {

            ResponseData.forEach(item => {
              const type = item.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                advertisers.push(item);
              }
            });

            const count = result.Count;
            offset = result.Offset;
            const nextOffset = offset + 500;
            if (count === 500) {
              offset = nextOffset;
              this.getAdvertiserData(offset, advertisers);
            }
            else {
              this.hideLoader();
              this.Advertisers_Array = advertisers;
            }
          } else {
           this.hideLoader();
            this.Advertisers_Array = [];
          }
        }
        else {
          this.hideLoader();
          this.Advertisers_Array = [];
        }
      }, error => {
        this.hideLoader();
     //   this.toastr.error('Something went wrong while getting advertiser data');
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      //this.toastr.error('An error occured while getting advertiser data');
      console.log(error);
    }
  }

  getAllReps() {
    try {
      this.advRepTerritoryMappingServiceService.getReps().subscribe(result => {
        if (result.StatusCode == '1') {
          this.Reps_Array = result.Data;
        } else {
          this.toastr.error(result.Message, 'Something went wrong!');
        }
      }, error => {
      //  this.toastr.error('An error occured while getting Reps data');
        console.log(error);
      });
    } catch (error) {
     // this.toastr.error('Something went wrong while getting Reps data');
      console.log(error);
    }
  }

  onChangeReps(RepsId, index) {
    this.fieldArray[index].TerritoryArray = [];
   this.fieldArray[index].Territory = "";
   this.GetTerritoryDataByReps(RepsId, index, true, 0);
    //this.GetTerritoryDataByReps(RepsId, index);
  }

  GetTerritoryDataByReps(RepsId, index,isonchangeReps, TerritoryId) {
    try {
      if (isNullOrUndefined(RepsId) == false) {
        if (RepsId != '') {
         // this.showLoader();
          this.advRepTerritoryMappingServiceService.getAllTerritories(RepsId).subscribe(result => {
            if (result.StatusCode == 1) {
           
              if (result.Data.length > 0) {
               this.fieldArray[index].TerritoryArray = result.Data;
               this.advertiserRepButtonName='Update';
              }

              if (isonchangeReps == false) {
                this.fieldArray[index].Territory = TerritoryId;
              }

            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            //this.toastr.error('Something went wrong while getting territory data');
            console.log(error);
          });
        }
      }
    } catch (error) {
     // this.toastr.error('An error occured while getting territory data');
      console.log(error);
    }
  }

  set_Advertiser_Name(advertiserId)
  {
    this.Advertisers_Array.forEach(item => {
      if (item.Id===advertiserId) {
        this.advertiserName=item.Name;
      }
    });
  }


  get_AdvertiserRepTerritoryMapping_List() {
    try {
      this.showLoader();
        this.advRepTerritoryMappingServiceService.getAllAdvertiserRepTerritoryMappingList().subscribe(result => {
          this.hideLoader();
          if (result.StatusCode == '1') {
            console.log(result.Data);
            this.advertisersRepTerritoryMappingList = result.Data;
          }
          console.log(this.advertisersRepTerritoryMappingList);
        }, error => {
          this.hideLoader();
          console.log(error);
        });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  deleteAdvertiserRepTerritoryMapping(objAdvertiserMapping) {
    try {
      if (confirm('Are you sure you want to delete this record?')) {
        const advertiserRepTerritoryMapId = objAdvertiserMapping.AdvertiserRepTerritoryMapId;
        if (!isNullOrUndefined(advertiserRepTerritoryMapId)) {
          try {
            this.advRepTerritoryMappingServiceService.deleteAdvertiserRepTerritoryMappingData(advertiserRepTerritoryMapId).subscribe(result1 => {
              if (result1.StatusCode === 1) {
                this.toastr.success('Deleted successfully.', 'Success!');
                this.get_AdvertiserRepTerritoryMapping_List();
              } else {
                this.toastr.error(result1.Message, 'Error!');
              }
            }, error => {
              console.log(error);
            });
          } catch (error) {
            console.log(error);
          }
        } else {
          console.log('Advertiser Rep Territory Mapping Id is null');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  //--------------------------------------//
  get_Advertiser_Data() {
    this.set_Advertiser_Name(this.selectedAdvertiser);
    // reset field array
    this.fieldArray = [
      {
        'Rep': "",
        'Territory': "",
        'Percentage': '',
        'RepsErrorMsg': '',
        'TerritoryErrorMsg': '',
        'PencentageErrorMsg': '',
        'TerritoryArray': []
      }
    ];

    try {
      if (this.selectedAdvertiser != null) {
     
        this.advRepTerritoryMappingServiceService.GetAdvertiser_ByAdvertiserId(this.websiteRoot, this.selectedAdvertiser).subscribe(result => {
          this.hideLoader();
          if (result.StatusCode == '1') {
            // set value
            var AdvertiserRepTerritoryList = result.Data;
            if (AdvertiserRepTerritoryList.length > 0) {
              // data found then empty the field array
              this.fieldArray = [];
              for (let i = 0; i < AdvertiserRepTerritoryList.length; i++) {
                this.fieldArray.push({
                  'Rep': AdvertiserRepTerritoryList[i].RepID,
                  'Territory': "",                  
                  'Percentage': AdvertiserRepTerritoryList[i].Percentage,
                  'RepsErrorMsg': '',
                  'TerritoryErrorMsg': '',
                  'PencentageErrorMsg': '',
                  'TerritoryArray': []
                });
                this.GetTerritoryDataByReps(AdvertiserRepTerritoryList[i].RepID, i, false, AdvertiserRepTerritoryList[i].TerritoryID);
              }
            }
          }
          else {
            this.toastr.error(result.ErrorMessage);
          }
        }, error => {
          this.hideLoader();
         // this.toastr.error('Something went wrong while getting advertiser-rep-territory mapping data');
          console.log(error);
        });
      }
    } catch (error) {
      this.hideLoader();
     // this.toastr.error('An error occured while getting advertiser-rep-territory mapping data');
      console.log(error);
    }
  }

  onSubmit(fieldArray_data) {
    var isLoopBreak = false;
    var t_fieldArray_data = [];

    if (this.AdvertiserRepTerritoryMappingForm.invalid) {
      return;
    }
    else {
      try {
        var totalPercentage = 0;
        // check rep-territory-percentage data for null or empty or zero
        for (let i = 0; i < fieldArray_data.length; i++) {
          if (isNullOrUndefined(fieldArray_data[i].Rep) || fieldArray_data[i].Rep == '') {
            //this.toastr.error('Reps data can not be null or empty');
            this.fieldArray[i].RepsErrorMsg = 'Select reps';
            isLoopBreak = true;
            //break;
          }
          else {
            this.fieldArray[i].RepsErrorMsg = '';
          }

          if (isNullOrUndefined(fieldArray_data[i].Territory) || fieldArray_data[i].Territory == '') {
            //this.toastr.error('Territory data can not be null or empty');
            this.fieldArray[i].TerritoryErrorMsg = 'Select territory';
            isLoopBreak = true;
            //break;
          }
          else {
            this.fieldArray[i].TerritoryErrorMsg = '';
          }

          if (isNullOrUndefined(fieldArray_data[i].Percentage) || fieldArray_data[i].Percentage === '') {
            //this.toastr.error('Percentage data can not be null or empty');
            this.fieldArray[i].PencentageErrorMsg = 'Enter percentage';
            isLoopBreak = true;
            //break;
          }

          else if (isNaN(fieldArray_data[i].Percentage)) {
            //this.toastr.error('Percentage data not valid');
            this.fieldArray[i].PencentageErrorMsg = 'Percentage data not valid';
            isLoopBreak = true;
            //break;
          }

          else if (parseInt(fieldArray_data[i].Percentage) <= 0) {
            //this.toastr.error('Percentage data can not be a zero or less');
            this.fieldArray[i].PencentageErrorMsg = 'Percentage data can not be a zero or less';
            isLoopBreak = true;
            //break;
          }
          else {
            this.fieldArray[i].PencentageErrorMsg = '';
          }
        }

        if (isLoopBreak == true) {
          return;
        }

        //----------------------------------------------

        // Make error message empty
        for (let i = 0; i < fieldArray_data.length; i++) {
          this.fieldArray[i].RepsErrorMsg = '';
          this.fieldArray[i].TerritoryErrorMsg = '';
          this.fieldArray[i].PencentageErrorMsg = '';
        }

        //----------------------------------------------

        // check rep - territory is repeated or not
        if (fieldArray_data.length >= 2) {
          const distinctRep = fieldArray_data.filter(
            (thing, i, arr) => arr.findIndex(t => t.Rep == thing.Rep) === i
          );

          if (fieldArray_data.length != distinctRep.length) {
            this.toastr.error('Duplicate reps cannot be selected');
            isLoopBreak = true;
            return;
          }

          const distinctTerritory = fieldArray_data.filter(
            (thing, i, arr) => arr.findIndex(t => t.Territory == thing.Territory) === i
          );

          if (fieldArray_data.length != distinctTerritory.length) {
            this.toastr.error('Duplicate territory cannot be selected');
            isLoopBreak = true;
            return;
          }
        }

        //----------------------------------------------
        //console.log(fieldArray_data);
        // check percentage distribution = 100 or not
        for (let i = 0; i < fieldArray_data.length; i++) {
          totalPercentage += parseFloat(fieldArray_data[i].Percentage);
        }

        if (totalPercentage != 100) {
          this.toastr.error('Total percentage distribution is not 100');
          isLoopBreak = true;
          return;
        }
        //----------------------------------------------
        if (isLoopBreak == false) {
         for (let i = 0; i < fieldArray_data.length; i++) {
            t_fieldArray_data.push({
              'RepID': fieldArray_data[i].Rep,
              'RepName':'',
              'TerritoryID': fieldArray_data[i].Territory,
              'TerritoryName':'',
              'Percentage': fieldArray_data[i].Percentage
            });
          }

          var formdata = {
            AdvertiserID: this.AdvertiserRepTerritoryMappingForm.value.Advertiser,
            AdvertiserName:this.advertiserName,
            ListRepTerritoryPercentage: t_fieldArray_data
          };
          this.showLoader();
          this.advRepTerritoryMappingServiceService.SaveAdvertiserRepTerritoryMapping(this.websiteRoot, formdata).subscribe(result => {
            this.hideLoader();
             console.log(result);
            this.isFormSubmitted = false;
            if (result.StatusCode == '1') {
              // display a success message
              this.toastr.success('Saved successfully');
              
             this.AdvertiserRepTerritoryMappingForm.reset();
             this.Advertisers_Array=[];
             this.get_AdvertiserRepTerritoryMapping_List();
              // reset field array
              this.fieldArray = [
                {
                  'Rep': "",
                  'Territory': "",
                  'Percentage': '',
                  'RepsErrorMsg': '',
                  'TerritoryErrorMsg': '',
                  'PencentageErrorMsg': '',
                  'TerritoryArray': []
                }
              ];
              //this.getAdvertiserData(0, []);
            }
            else {
              // display an error message
              this.toastr.error('Something went wrong!');
            }
            this.advertiserRepButtonName = 'Submit';
          }, error => {
            this.hideLoader();
            console.log(error);
          });
        }
      } catch (error) {
       this.hideLoader();
        console.log(error);
      }
    }
  }

  //--------------------------------------//

  addNewFieldValue() {
    this.fieldArray.push({
      'Rep': "",
      'Territory': "",
      'Percentage': "",
      'RepsErrorMsg': '',
      'TerritoryErrorMsg': '',
      'PencentageErrorMsg': '',
      'TerritoryArray': []
    });
    this.updateDynamicCommissionAdvertiserRepMapping();
  }

  updateDynamicCommissionAdvertiserRepMapping() {
    try {
      const commissionData = this.fieldArray.length === 0 ? 100 : (100 / (this.fieldArray.length));
      const commissionDataTemp = parseFloat(commissionData.toString()).toFixed(2);
      for (let i = 0; i < this.fieldArray.length; i++) {
        console.log(commissionDataTemp);
        this.fieldArray[i].Percentage=commissionDataTemp;
      }
      if(this.fieldArray.length===3)
      {
        this.fieldArray[this.fieldArray.length-1].Percentage="33.34";
      }
    } catch (error) {
      console.log(error);
    }
  }

  changePercentage(insertedPer,actualPer)
  {
    try {
      let updateValue = insertedPer;
      console.log("changePer:"+ insertedPer);
      const remainingPercentage = 100-updateValue;
      console.log("remainingPercentage:"+ remainingPercentage);
      const commissionData = this.fieldArray.length === 0 ? 100 : (remainingPercentage / (this.fieldArray.length-1));
      const commissionDataTemp = parseFloat(commissionData.toString()).toFixed(2);
      for (let i = 0; i < this.fieldArray.length; i++) {
        if(this.fieldArray[i].Percentage ===actualPer)
        {}
        else{
          this.fieldArray[i].Percentage=commissionDataTemp;
        }
      }
      // if(this.fieldArray.length===3)
      // {
      //   this.fieldArray[this.fieldArray.length-1].Percentage="33.34";
      // }
    } catch (error) {
      console.log(error);
    }
  }

  deleteFieldValue(index) {
    this.fieldArray.splice(index, 1);
    this.updateDynamicCommissionAdvertiserRepMapping();
  }

   //--------------------------------//
 //---------------- Loader ----------------
 showLoader() {
  this.appComponent.isLoading = true;
}

hideLoader() {
  setTimeout(() => {
    this.appComponent.isLoading = false;
  }, 500);
}

setClearAllSearchFields(popover) {
  try {
    if (popover.isOpen()) {
      popover.close();
    }
  } catch (error) {
    console.log(error);
  }
}

onChangeAdvertiserFilter(selectedValue)
{
  if(selectedValue==="All")
  {
    $("#divAdvertiserFilter").hide();
  }
  else{
    $("#divAdvertiserFilter").show();
  }
}

clearAdvertiserFilter()
{
  //$("#txtAdvertiserFilterValue").val("");
  (document.getElementById("txtAdvertiserFilterValue") as HTMLInputElement).value="";
}

applyAdvertiserFilter(popover)
{
    let advertiserFilterValue = (document.getElementById("txtAdvertiserFilterValue") as HTMLInputElement).value; //$("#txtAdvertiserFilterValue").val();
    let advertiserFilterOption=(document.getElementById("cboFilterType") as HTMLSelectElement).value;//$("#cboFilterType").val();
    this.AdvertiserRepTerritoryMappingForm.controls['Advertiser'].setValue(null);
    if(advertiserFilterValue.trim()==="" && advertiserFilterOption!=="All")
    {
      this.toastr.error("Please enter value", 'Error');
    }
    else{
      try 
        {
          this.showLoader();
          const hasNext = true;
          const offset = 0;
          const advertisers = [];
          this.getAdvertiserDataViaFilterSearch(hasNext, offset, advertisers, true,advertiserFilterOption, advertiserFilterValue);
          this.hideLoader();
          if (popover.isOpen()) {
            popover.close();
          }
        }
        catch(error)
        {
          console.log(error);
        }
    }
}

getAdvertiserDataViaFilterSearch(hasNext: boolean, offset, advertisers: any[], issetloader: boolean,filterOption,filterValue) {
  try {
    if (issetloader === true) {
      this.showLoader();
    }
    this.advRepTerritoryMappingServiceService.getAdvertiserByFilterSearch(this.websiteRoot, offset,filterOption,filterValue).subscribe(result => {
      if (result != null && result !== undefined && result !== '') {
        const ItemData = result.Items.$values;
        if (ItemData.length > 0) {
          ItemData.forEach(itemdatavalue => {
            const type = itemdatavalue.$type;
            if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
              advertisers.push(itemdatavalue);
            }
          });
          const totalCount = result.TotalCount;
          const count = result.Count;
          hasNext = result.HasNext;
          offset = result.Offset;
          const limit = result.Limit;
          // tslint:disable-next-line:radix
          const nextOffset = parseInt(offset) + 500;
          if (count === 500) {
            offset = nextOffset;
            this.getAdvertiserDataViaFilterSearch(hasNext, offset, advertisers, true,filterOption,filterValue);
          } else {
            this.hideLoader();
            this.Advertisers_Array = advertisers;
          }
        } else {
          this.hideLoader();
          this.Advertisers_Array = advertisers;
        }
      } else {
        this.hideLoader();
        this.Advertisers_Array = [];
      }
    }, error => {
      this.hideLoader();
      console.log(error);
    });
  } catch (error) {
    this.hideLoader();
    console.log(error);
  }
}
//-------------- End Loader --------------
}