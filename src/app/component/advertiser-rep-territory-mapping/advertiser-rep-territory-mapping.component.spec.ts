import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertiserRepTerritoryMappingComponent } from './advertiser-rep-territory-mapping.component';

describe('AdvertiserRepTerritoryMappingComponent', () => {
  let component: AdvertiserRepTerritoryMappingComponent;
  let fixture: ComponentFixture<AdvertiserRepTerritoryMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertiserRepTerritoryMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserRepTerritoryMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
