import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertiserAgencyMappingComponent } from './advertiser-agency-mapping.component';

describe('AdvertiserAgencyMappingComponent', () => {
  let component: AdvertiserAgencyMappingComponent;
  let fixture: ComponentFixture<AdvertiserAgencyMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertiserAgencyMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserAgencyMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
