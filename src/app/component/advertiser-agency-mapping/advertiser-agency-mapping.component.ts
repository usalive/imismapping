import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { isNullOrUndefined } from 'util';
import { NgxSpinnerService } from "ngx-spinner";
import { environment } from '../../../environments/environment';
import { AdvAgnMappingServiceService } from '../../Service/adv-agn-mapping-service.service';
import { AppComponent } from '../../app.component';
declare var $, jQuery: any;

@Component({
  selector: 'app-advertiser-agency-mapping',
  templateUrl: './advertiser-agency-mapping.component.html',
  styleUrls: ['./advertiser-agency-mapping.component.css']
})
export class AdvertiserAgencyMappingComponent implements OnInit {

  selectedAdvertiser: any;
  selectedAgency: any;

  AdvertiserAgencyMappingForm: FormGroup;
  isUpdate = false;
  Advertisers_Agencies_Array = [];
  Company2_Array = [];
  advertisersAgencyMappingList = [];

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  advertiserName='';
  agencyName='';
  advertiserAgencyButtonName='Submit';

  constructor(private formbuilder: FormBuilder,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private appComponent: AppComponent,
    private advAgnMappingServiceService: AdvAgnMappingServiceService) {


    this.AdvertiserAgencyMappingForm = this.formbuilder.group({
      Advertiser: ['', Validators.required],
      Agency: ['', Validators.required],
      Relation: ['', Validators.required]
    });
  }

  isFormSubmitted = false;

  ngOnInit(): void {
    this.isUpdate = false;
    this.Advertisers_Agencies_Array = [];
    this.getToken();
    //this.getAdvertiserData(0, []);
    this.get_AdvertiserAgencyMapping_List();
  }

  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;

      var pathArray = this.websiteRoot.split('/');

      var protocol = pathArray[0];
      var host = pathArray[2];

      this.websiteRoot = protocol + '//' + host + '/';

      if (this.websiteRoot.startsWith("http://"))
        this.websiteRoot = this.websiteRoot.replace('http://', 'https://');

      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }
  }

 

  getAdvertiserData(offset: number, advertisers: any[]) {
    try {
     // this.spinner.show();.
     this.showLoader();
      this.advAgnMappingServiceService.GetAdvertiser(this.websiteRoot, offset).subscribe(result => {
        if (isNullOrUndefined(result) == false) {
          const ResponseData = result.Items.$values;
          if (ResponseData.length > 0) {
            ResponseData.forEach(item => {
              const type = item.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                advertisers.push(item);
              }
            });
            const count = result.Count;
            offset = result.Offset;
            const nextOffset = offset + 500;
            if (count === 500) {
              offset = nextOffset;
              this.getAdvertiserData(offset, advertisers);
            }
            else {
              this.hideLoader();
              this.Advertisers_Agencies_Array = advertisers;
            }
            console.log(this.Advertisers_Agencies_Array);
          } else {
            this.hideLoader();
            this.Advertisers_Agencies_Array = [];
          }
        }
        else {
          this.hideLoader();
          this.Advertisers_Agencies_Array = [];
        }
      }, error => {
       this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  set_Advertiser_Name(advertiserId)
  {
    this.Advertisers_Agencies_Array.forEach(item => {
      if (item.Id===advertiserId) {
        this.advertiserName=item.Name;
      }
    });
  }

  //--------------------------------//

  get_Advertiser_Data() {
    try {
      if (this.selectedAdvertiser != null) {
        this.set_Advertiser_Name(this.selectedAdvertiser);
       this.showLoader();
        this.advAgnMappingServiceService.GetAdvertiser_ByAdvertiserId(this.websiteRoot, this.selectedAdvertiser).subscribe(result => {
          this.hideLoader();
          if (result.StatusCode == '1') {
            this.isUpdate = true;
            this.AdvertiserAgencyMappingForm.controls['Agency'].setValue(result.Data.AgencyID);
            this.AdvertiserAgencyMappingForm.controls['Relation'].setValue(result.Data.Relation);
            this.advertiserAgencyButtonName = 'Update';
          }
          else {
            this.isUpdate = false;
            this.AdvertiserAgencyMappingForm.controls['Agency'].setValue(null);
            this.AdvertiserAgencyMappingForm.controls['Relation'].setValue("Advertiser - Agency");
          //  this.toastr.error(result.ErrorMessage);
          }
        }, error => {
          this.hideLoader();
        //  this.toastr.error('Something went wrong while getting advertiser-agency mapping data');
          console.log(error);
        });
      }
      else {
        this.hideLoader();
        this.AdvertiserAgencyMappingForm.controls['Agency'].setValue(null);
        this.AdvertiserAgencyMappingForm.controls['Relation'].setValue("Advertiser - Agency");
      }
    } catch (error) {
      this.hideLoader();
     // this.toastr.error('An error occured while getting advertiser-agency mapping data');
      console.log(error);
    }
  }

  AddUpdateAdvertiserAgencyMappingSubmit() {
    this.isFormSubmitted = true;
    if (this.AdvertiserAgencyMappingForm.invalid) {
      return;
    }
    else {
      console.log(this.AdvertiserAgencyMappingForm.controls['Advertiser'].value +"-"+ this.AdvertiserAgencyMappingForm.controls['Advertiser'].value);
      if(this.AdvertiserAgencyMappingForm.controls['Advertiser'].value===this.AdvertiserAgencyMappingForm.controls['Agency'].value)
      {
        this.toastr.error("Please select different companies for this relationship", 'Error!');
      }
      else{
        this.Company2_Array.forEach(item => {
          if (item.Id===this.AdvertiserAgencyMappingForm.controls['Agency'].value) {
            this.agencyName=item.Name;
          }
        });
        var formdata = {
          'AdvertiserID': this.AdvertiserAgencyMappingForm.controls['Advertiser'].value,
          'AdvertiserName': this.advertiserName,
          'AgencyID': this.AdvertiserAgencyMappingForm.controls['Agency'].value,
          'AgencyName': this.agencyName,
          'Relation': this.AdvertiserAgencyMappingForm.controls['Relation'].value,
          'IsUpdate': this.isUpdate
        };
       //console.log(formdata);
       try {
        this.advAgnMappingServiceService.SaveAdvertiserAgencyMapping(this.websiteRoot, formdata).subscribe(result => {
         this.showLoader();
          this.isFormSubmitted = false;
          if (result.StatusCode == '1') {
            this.toastr.success('Saved successfully');
            this.AdvertiserAgencyMappingForm.reset();
            this.AdvertiserAgencyMappingForm.controls['Advertiser'].setValue(null);
            this.AdvertiserAgencyMappingForm.controls['Agency'].setValue(null);
            this.AdvertiserAgencyMappingForm.controls['Relation'].setValue("Advertiser - Agency");
            // display a success message
           
            this.get_Advertiser_Data();
            this.get_AdvertiserAgencyMapping_List();
            this.advertiserName='';
            this.agencyName='';
          }
          else {
            // display an error message
            this.hideLoader();
            this.toastr.error(result.Message);
          }
        }, error => {
          this.hideLoader();
          console.log(error);
        });
        this.advertiserAgencyButtonName='Submit';
      } catch (error) {
        this.hideLoader();
         //this.spinner.hide();
       // this.toastr.error('An error occured while saving advertiser-agency- mapping data');
        console.log(error);
      }
      }
    }
  }

  get_AdvertiserAgencyMapping_List() {
    try {
      this.showLoader();
        this.advAgnMappingServiceService.GetAdvertiserAgencyMappingList().subscribe(result => {
          this.hideLoader();
          if (result.StatusCode == '1') {
            this.advertisersAgencyMappingList = result.Data;
          }
          //console.log(this.advertisersAgencyMappingList);
        }, error => {
          this.hideLoader();
          console.log(error);
        });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  deleteAdvertiserAgencyMapping(objAdvertiserAgency) {
    try {
      if (confirm('Are you sure you want to delete this record?')) {
        const advertiserAgencyMapId = objAdvertiserAgency.AdvertiserAgencyMapId;
        if (!isNullOrUndefined(advertiserAgencyMapId)) {
          try {
            this.advAgnMappingServiceService.deleteAdvertiserAgencyMappingData(advertiserAgencyMapId).subscribe(result1 => {
              if (result1.StatusCode === 1) {
                this.toastr.success('Deleted successfully.', 'Success!');
                this.get_AdvertiserAgencyMapping_List();
              } else {
                this.toastr.error(result1.Message, 'Error!');
              }
            }, error => {
              console.log(error);
            });
          } catch (error) {
            console.log(error);
          }
        } else {
          console.log('Advertiser Agency Mapping Id is null');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }


  //--------------------------------//
 //---------------- Loader ----------------
 showLoader() {
  this.appComponent.isLoading = true;
}

hideLoader() {
  setTimeout(() => {
    this.appComponent.isLoading = false;
  }, 500);
}
//-------------- End Loader --------------

setClearAllSearchFields(popover) {
  try {
    if (popover.isOpen()) {
      popover.close();
    }
  } catch (error) {
    console.log(error);
  }
}

onChangeCompany1Filter(selectedValue)
{
  if(selectedValue==="All")
  {
    $("#divCompany1Filter").hide();
  }
  else{
    $("#divCompany1Filter").show();
  }
}

onChangeCompany2Filter(selectedValue)
{
  if(selectedValue==="All")
  {
    $("#divCompany2Filter").hide();
  }
  else{
    $("#divCompany2Filter").show();
  }
}

clearCompany1Filter()
{
  //$("#txtCompany1FilterValue").val("");
  (document.getElementById("txtCompany1FilterValue") as HTMLInputElement).value="";
}

clearCompany2Filter()
{
  (document.getElementById("txtCompany2FilterValue") as HTMLInputElement).value="";
}

applyCompany1Filter(popover)
{
    let company1FilterValue = (document.getElementById("txtCompany1FilterValue") as HTMLInputElement).value;
    let company1FilterOption = (document.getElementById("cboCompany1FilterType") as HTMLSelectElement).value//$("#cboCompany1FilterType").val();
    if(company1FilterValue.trim()==="" && company1FilterOption!=="All")
    {
      this.toastr.error("Please enter value", 'Error');
    }
    else{
        try 
        {
          this.showLoader();
          const hasNext = true;
          const offset = 0;
          const company1 = [];
          this.getCompanyData(offset, company1,company1FilterOption, company1FilterValue);
          this.hideLoader();
          if (popover.isOpen()) {
            popover.close();
          }
        }
        catch(error)
        {
          console.log(error);
        }
    }
}

applyCompany2Filter(popover)
{
    let company2FilterValue = (document.getElementById("txtCompany2FilterValue") as HTMLInputElement).value;//$("#txtCompany2FilterValue").val();
    let company2FilterOption =(document.getElementById("cboCompany2FilterType") as HTMLSelectElement).value;// $("#cboCompany2FilterType").val();
    if(company2FilterValue.trim()==="" && company2FilterOption!=="All")
    {
      this.toastr.error("Please enter value", 'Error');
    }
    else{
        try 
        {
          this.showLoader();
          const hasNext = true;
          const offset = 0;
          const company2 = [];
          this.getCompany2Data(offset, company2,company2FilterOption, company2FilterValue);
          this.hideLoader();
          if (popover.isOpen()) {
            popover.close();
          }
        }
        catch(error)
        {
          console.log(error);
        }
    }
}

getCompanyData(offset: number, company1: any[],filterOption, filterValue) {
  try {
   // this.spinner.show();.
   this.showLoader();
    this.advAgnMappingServiceService.getAdvertiserByFilterSearch(this.websiteRoot, offset,filterOption,filterValue).subscribe(result => {
      if (isNullOrUndefined(result) == false) {
        const ResponseData = result.Items.$values;
        if (ResponseData.length > 0) {
          ResponseData.forEach(item => {
            const type = item.$type;
            if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
              company1.push(item);
            }
          });
          const count = result.Count;
          offset = result.Offset;
          const nextOffset = offset + 500;
          if (count === 500) {
            offset = nextOffset;
            this.getCompanyData(offset, company1,filterOption,filterValue);
          }
          else {
            this.hideLoader();
            this.Advertisers_Agencies_Array = company1;
          }
        } else {
          this.hideLoader();
          this.Advertisers_Agencies_Array = [];
        }
      }
      else {
        this.hideLoader();
        this.Advertisers_Agencies_Array = [];
      }
    }, error => {
     this.hideLoader();
      console.log(error);
    });
  } catch (error) {
    this.hideLoader();
    console.log(error);
  }
}

getCompany2Data(offset: number, company2: any[],filterOption, filterValue) {
  try {
   // this.spinner.show();.
   this.showLoader();
    this.advAgnMappingServiceService.getAdvertiserByFilterSearch(this.websiteRoot, offset,filterOption,filterValue).subscribe(result => {
      if (isNullOrUndefined(result) == false) {
        const ResponseData = result.Items.$values;
        if (ResponseData.length > 0) {
          ResponseData.forEach(item => {
            const type = item.$type;
            if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
              company2.push(item);
            }
          });
          const count = result.Count;
          offset = result.Offset;
          const nextOffset = offset + 500;
          if (count === 500) {
            offset = nextOffset;
            this.getCompany2Data(offset, company2,filterOption,filterValue);
          }
          else {
            this.hideLoader();
            this.Company2_Array = company2;
          }
        } else {
          this.hideLoader();
          this.Company2_Array = [];
        }
      }
      else {
        this.hideLoader();
        this.Company2_Array = [];
      }
    }, error => {
     this.hideLoader();
      console.log(error);
    });
  } catch (error) {
    this.hideLoader();
    console.log(error);
  }
}
  

}
