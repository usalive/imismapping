import { TestBed } from '@angular/core/testing';

import { AdvRepTerritoryMappingServiceService } from './adv-rep-territory-mapping-service.service';

describe('AdvRepTerritoryMappingServiceService', () => {
  let service: AdvRepTerritoryMappingServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdvRepTerritoryMappingServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
