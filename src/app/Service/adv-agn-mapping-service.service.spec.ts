import { TestBed } from '@angular/core/testing';

import { AdvAgnMappingServiceService } from './adv-agn-mapping-service.service';

describe('AdvAgnMappingServiceService', () => {
  let service: AdvAgnMappingServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdvAgnMappingServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
