import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdvAgnMappingServiceService {

  authToken = environment.token;
  apiBaseURL = environment.apiBaseURL;
 

  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  public getheaders: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });

    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });

    this.getheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
  }

  GetAdvertiser(websiteRoot, Offset): Observable<any> {
    return this.http.get(websiteRoot + '/api/Party?limit=500&Offset=' + Offset, { headers: this.getheaders });
  }

  getAdvertiserByFilterSearch(websiteRoot, Offset,filterOption,filterValue): Observable<any> {
    try{
      if(filterOption==="All")
      {
        return this.http.get(websiteRoot + '/api/Party?limit=500&Offset=' + Offset, { headers: this.getheaders });
      }
      else{
        if(filterOption==="Equals")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=eq:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        }
        if(filterOption==="Contains")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=contains:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        }
        if(filterOption==="StartsWith")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=startsWith:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        } 
        if(filterOption==="EndsWith")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=endsWith:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        }
      }
    }
    catch(error)
    {
      console.log(error);
    }
    
  }

  GetAdvertiser_ByAdvertiserId(websiteRoot, advertiserId): Observable<any> {
    return this.http.get(this.apiBaseURL + '/advertiseragencymapping/Get/' + advertiserId, { headers: this.getheaders });
  }

  SaveAdvertiserAgencyMapping(websiteRoot, formdata: any): Observable<any> {
    var data = JSON.stringify(formdata);
    return this.http.post(this.apiBaseURL + '/advertiseragencymapping/AddUpdate', data, { headers: this.sfheaders });
  }

  GetAdvertiserAgencyMappingList():Observable<any>
  {
    return this.http.get(this.apiBaseURL+'/advertiseragencymapping',{headers:this.getheaders});
  }

  deleteAdvertiserAgencyMappingData(advertiserAgencyMapId): Observable<any> {
    try {
      return this.http.delete(`${environment.apiBaseURL}advertiseragencymapping/` +advertiserAgencyMapId, { headers: this.getheaders });
    } catch (error) {
      console.log(error);
    }
  }
}
