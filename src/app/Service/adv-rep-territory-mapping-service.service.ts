import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdvRepTerritoryMappingServiceService {
  authToken = environment.token;
  apiBaseURL = environment.apiBaseURL;
  reps_API = this.apiBaseURL + '/Reps';
  partyApi = this.apiBaseURL + '/party?limit=500&offset=';
  repTerritories_API = this.apiBaseURL + '/RepTerritories';

  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  public getheaders: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });

    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });

    this.getheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
  }

  GetAdvertiser(websiteRoot, Offset): Observable<any> {
    return this.http.get(websiteRoot + '/api/Party?limit=500&Offset=' + Offset, { headers: this.getheaders });
  }

  getAdvertiserByFilterSearch(websiteRoot, Offset,filterOption,filterValue): Observable<any> {
    try{
      if(filterOption==="All")
      {
        return this.http.get(websiteRoot + '/api/Party?limit=500&Offset=' + Offset, { headers: this.getheaders });
      }
      else{
        if(filterOption==="Equals")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=eq:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        }
        if(filterOption==="Contains")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=contains:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        }
        if(filterOption==="StartsWith")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=startsWith:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        } 
        if(filterOption==="EndsWith")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=endsWith:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        }
      }
    }
    catch(error)
    {
      console.log("getAdvertiserByFilterSearch:"+error);
    }
    
  }

  getReps(): Observable<any> {
    return this.http.get(this.reps_API + '/GetRepsForOrderCreation');
  }

  getAllTerritories(id): Observable<any> {
    return this.http.get(this.repTerritories_API + '/GetByRep/' + id);
  }

  getAllAdvertiserRepTerritoryMappingList():Observable<any>
  {
    return this.http.get(this.apiBaseURL+'advertiserrepterritorymapping',{headers:this.getheaders});
  }

  deleteAdvertiserRepTerritoryMappingData(advertiserRepTerritoryMapId): Observable<any> {
    try {
      return this.http.delete(`${environment.apiBaseURL}advertiserrepterritorymapping/` +advertiserRepTerritoryMapId, { headers: this.getheaders });
    } catch (error) {
      console.log(error);
    }
  }

  /* Newly create mvc api call  */

  GetAdvertiser_ByAdvertiserId(websiteRoot, advertiserId): Observable<any> {
    return this.http.get(this.apiBaseURL + 'advertiserrepterritorymapping/Get/' + advertiserId, { headers: this.getheaders });
  }

  SaveAdvertiserRepTerritoryMapping(websiteRoot, formdata: any): Observable<any> {
    var data = JSON.stringify(formdata);
    return this.http.post(this.apiBaseURL + 'advertiserrepterritorymapping/AddUpdate', data, { headers: this.sfheaders });
  }
}
