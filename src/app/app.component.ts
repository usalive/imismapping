import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
@Component({
  selector: 'asi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';;

  AdvertiserAgencyTab = true;
  AdvertiserAgencyDisable = false;

  AdvertiserRepTerritoryTab = false;
  AdvertiserRepTerritoryDisable = false;
  isLoading = true;
  constructor() {
  }

  ngOnInit(): void {
    const url = window.location.href;
    this.websiteRoot = environment.websiteRoot;
    var pathArray = this.websiteRoot.split( '/' );
    var protocol = pathArray[0];
    var host = pathArray[2];
    this.websiteRoot = protocol + '//' + host + '/';
    if (this.websiteRoot.startsWith("http://"))
    this.websiteRoot = this.websiteRoot.replace('http', 'https');
    this.reqVerificationToken = environment.token;
    this.imgurl = environment.imageUrl;
    if (url.search('AdvertiserAgency') > 1) {
      this.setAdvertiserAgencyTabActive();
    } else if (url.search('AdvertiserRepTerritory') > 1) {
      this.setAdvertiserRepTerritoryTabActive();
    }
  }

  // Tab click function
  setAdvertiserAgencyTabActive() {
    let url = window.location.href;

    if (url.indexOf('#') > -1) {
      url = url.substring(0, url.lastIndexOf('#'));
      // do nothing
    }

    location.replace(url + '#/AdvertiserAgency/Mapping');
    try {
      this.AdvertiserAgencyTab = true;
      this.AdvertiserAgencyDisable = false;

      this.AdvertiserRepTerritoryTab = false;
      this.AdvertiserRepTerritoryDisable = false;
    } catch (error) {
      console.log(error);
    }
  }

  setAdvertiserRepTerritoryTabActive() {
    let url = window.location.href;

    if (url.indexOf('#') > -1) {
      url = url.substring(0, url.lastIndexOf('#'));
      // do nothing
    }

    location.replace(url + '#/AdvertiserRepTerritory/Mapping');
    try {
      this.AdvertiserAgencyTab = false;
      this.AdvertiserAgencyDisable = false;

      this.AdvertiserRepTerritoryTab = true;
      this.AdvertiserRepTerritoryDisable = false;
    } catch (error) {
      console.log(error);
    }
  }
}
