﻿function ScrollTo(clientId) {
    if (typeof jQuery === "undefined") {
        return;
    }

    var element = jQuery("#" + clientId);
    if (element.length != 0) {
        jQuery('html, body').animate({
            scrollTop: element.offset().top
        }, 1000, 'swing');
    }

}